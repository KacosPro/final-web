# Web Programming class, last exam code

This is the unmodified code of the last iteration of our web programming class. Please bear in mind that we didn't know about the `fetch` API, closures, hoisting, scope, or anything about the "wonderful" world of JavaScript.

Just to add some context we used to share our code in Google Drive and name the iterations as <i>final-web</i>, <i>final-web final</i>, <i>final-web 3</i> 🤦‍♂️

**final-web 3** being the last one, and the date of the document was 20 de mayo de 2015 10:17, I can only imagine what the previous iterations looked like 😰

## Authors 👨🏻‍💻

 - [Chucho Marin](https://twitter.com/themarinn)
 - [Carlos Proaño](https://twitter.com/KacosPro)