function drawWorld() {
	//Canvas
	var canvas = document.getElementById('canvas');
	//canvas.width = window.innerWidth-50;
	//canvas.height = window.innerHeight-50;
	var context = canvas.getContext('2d');
	var canvas2 = document.getElementById('canvas2');
	var context2 = canvas2.getContext('2d');

	url= 'https://fast-ridge-1427.herokuapp.com/games/1' ;
	options={url:url};
	makeRequest(options);


	function makeRequest(url) {
		var xhr = new XMLHttpRequest();

		options = options || {};
		options.success = options.success || function() {
			//obj arreglo con los datos sacados de la URL
			obj = JSON.parse(this.responseText);

			//Variables
			var background = new Image();
			var characterImage = new Image();
			var music = new Audio('https://fast-ridge-1427.herokuapp.com/audio/tick.m4a');
			var sx = 0;
			var sy = 0;
			var sWidth = 0;
			var sHeight = 0;
			var active = 0;
			var playing = true;
			var menushow = true;
			var direction = []
			var positionX = [];
			var positionY = [];

			//Música
			music.loop = true;
			music.play();

	//Dibujar mundo al cargar
	background.onload = function() {
		for (var i=0;i<canvas.width;i++){
			for (var j=0;j<canvas.height;j++){
				context.drawImage(background, i*50, j*50);
			}
		}
	}
	characterImage.onload = function() {
		for (var i = 0; i < obj.Characters.length; i++) {
			var dice = Math.floor((Math.random() * 4 + 0));
			direction[i] = dice;
			if (dice == 0) {
				context.drawImage(characterImage,
				obj.Characters[i].positions.down.x,
				obj.Characters[i].positions.down.y,
				obj.Characters[i].positions.down.width,
				obj.Characters[i].positions.down.height,
				positionX[i] = Math.floor((Math.random() * (canvas.width-19)) + 1),
				positionY[i] = Math.floor((Math.random() * (canvas.height-32)) + 1), 19, 32);
			}else if (dice == 1) {
				context.drawImage(characterImage,
				obj.Characters[i].positions.up.x,
				obj.Characters[i].positions.up.y,
				obj.Characters[i].positions.up.width,
				obj.Characters[i].positions.up.height,
				positionX[i] = Math.floor((Math.random() * (canvas.width-19)) + 1),
				positionY[i] = Math.floor((Math.random() * (canvas.height-32)) + 1), 19, 32);
			}else if (dice == 2) {
				context.drawImage(characterImage,
				obj.Characters[i].positions.left.x,
				obj.Characters[i].positions.left.y,
				obj.Characters[i].positions.left.width,
				obj.Characters[i].positions.left.height,
				positionX[i] = Math.floor((Math.random() * (canvas.width-19)) + 1),
				positionY[i] = Math.floor((Math.random() * (canvas.height-32)) + 1), 19, 32);
			}else {
				context.drawImage(characterImage,
				obj.Characters[i].positions.right.x,
				obj.Characters[i].positions.right.y,
				obj.Characters[i].positions.right.width,
				obj.Characters[i].positions.right.height,
				positionX[i] = Math.floor((Math.random() * (canvas.width-19)) + 1),
				positionY[i] = Math.floor((Math.random() * (canvas.height-32)) + 1), 19, 32);
			};
		};
	}

	putText(obj.Characters[active].name);

	background.src = obj.World.image.src;
	characterImage.src = obj.Characters[0].image.src;

	function putText(text) {
		context.font="15px Verdana";
		// Create gradient
		var gradient=context.createLinearGradient(0, 0, canvas.width, 0);
		gradient.addColorStop("0","magenta");
		gradient.addColorStop("0.5","blue");
		gradient.addColorStop("1.0","red");
		// Fill with gradient
		context.fillStyle=gradient;
		context.fillText(text, canvas.width-100, 15);
	}

	function drawHelp(){
		context2.fillStyle = "red";
		context2.rect((canvas2.width/5), (canvas2.height/5), 200, 100);
		context2.fill();  
		context2.stroke();
		context2.fillStyle = "white";
  		context2.font = " 7px Arial";
  		context2.textAlign = "start";      
  		var ycoord= 56;
  		var tecla = 8;
		context2.fillText("S = Prender/Apagar Musica",65, 40); 
  		context2.fillText("H = Ayuda",65, 48);
  		for(i=0;i<obj.Characters.length;i++){
  			context2.fillText(tecla+" = "+obj.Characters[i].name,65, ycoord);
  			ycoord= ycoord+8;
  			tecla=tecla+1
  			if (tecla==10){
  				tecla=0;
  			}
  		}
	}

	function paint() {
		var lastPress = null;

		//Teclado
		document.addEventListener('keydown', function(evt){
			lastPress=evt.keyCode;
			switch(lastPress) {
				//left
				case 37:
				if (positionX[active]>0) {
					positionX[active]-=1;
					sx = obj.Characters[active].positions.left.x;
					sy = obj.Characters[active].positions.left.y;
					sWidth = obj.Characters[active].positions.left.width;
					sHeight = obj.Characters[active].positions.left.height;
				};
				break;
				//up
				case 38:
				if (positionY[active]>0) {
					positionY[active]-=1;
					sx = obj.Characters[active].positions.up.x;
					sy = obj.Characters[active].positions.up.y;
					sWidth = obj.Characters[active].positions.up.width;
					sHeight = obj.Characters[active].positions.up.height;
				};
				break;
				//right
				case 39:
				if (positionX[active]<(canvas.width-19)) {
					positionX[active]+=1;
					sx = obj.Characters[active].positions.right.x;
					sy = obj.Characters[active].positions.right.y;
					sWidth = obj.Characters[active].positions.right.width;
					sHeight = obj.Characters[active].positions.right.height;
				};
				break;
				//down
				case 40:
				if (positionY[active]<(canvas.height-32)) {
					positionY[active]+=1;
					sx = obj.Characters[active].positions.down.x;
					sy = obj.Characters[active].positions.down.y;
					sWidth = obj.Characters[active].positions.down.width;
					sHeight = obj.Characters[active].positions.down.height;
				};
				break;
				case 48:
				if (obj.Characters.length>=10) {
					active = 9;
				};
				break;
				case 49:
				if (1<obj.Characters.length>=1) {
					active = 0;
				};
				break;
				case 50:
				if (obj.Characters.length>=2) {
					active = 1;
				};
				break;
				case 51:
				if (obj.Characters.length>=3) {
					active = 2;
				};
				break;
				case 52:
				if (obj.Characters.length>=4) {
					active = 3;
				};
				break;
				case 53:
				if (obj.Characters.length>=5) {
					active = 4;
				};
				break;
				case 54:
				if (obj.Characters.length>=6) {
					active = 5;
				};
				break;
				case 55:
				if (obj.Characters.length>=7) {
					active = 6;
				};
				break;
				case 56:
				if (obj.Characters.length>=8) {
					active = 7;
				};
				break;
				case 57:
				if (obj.Characters.length>=9) {
					active = 8;
				};
				break;
				case 72:

				if(menushow){
					drawHelp();	
					menushow=false;
				}else{
					context2.clearRect(0,0,canvas2.width, canvas2.height); 
					menushow=true;
					}  
					break;
				case 83:
				if (playing) {
					music.pause();
					playing = false;
				}else{
					music.play();
					playing = true;
				}
				break;
			}
			for (var i=0;i<canvas.width;i++){
				for (var j=0;j<canvas.height;j++){
					context.drawImage(background, i*50, j*50);
				}
			}
			for (var i = 0; i < obj.Characters.length; i++) {
				if (direction[i] == 0) {
					if (i != active) {
						context.drawImage(characterImage,
						obj.Characters[i].positions.down.x,
						obj.Characters[i].positions.down.y,
						obj.Characters[i].positions.down.width,
						obj.Characters[i].positions.down.height,
						positionX[i], positionY[i], 19, 32);
					};
				}else if (direction[i] == 1) {
					if (i != active) {
						context.drawImage(characterImage,
						obj.Characters[i].positions.up.x,
						obj.Characters[i].positions.up.y,
						obj.Characters[i].positions.up.width,
						obj.Characters[i].positions.up.height,
						positionX[i], positionY[i], 19, 32);
					};
				}else if (direction[i] == 2) {
					if (i != active) {
						context.drawImage(characterImage,
						obj.Characters[i].positions.left.x,
						obj.Characters[i].positions.left.y,
						obj.Characters[i].positions.left.width,
						obj.Characters[i].positions.left.height,
						positionX[i], positionY[i], 19, 32);
					};
				}else {
					if (i != active) {
						context.drawImage(characterImage,
						obj.Characters[i].positions.right.x,
						obj.Characters[i].positions.right.y,
						obj.Characters[i].positions.right.width,
						obj.Characters[i].positions.right.height,
						positionX[i], positionY[i], 19, 32);
					};
				};
			};

			context.drawImage(characterImage, sx, sy, sWidth, sHeight,
				positionX[active], positionY[active], 19, 32);
			if (obj.Characters.length>=active) {
				putText(obj.Characters[active].name);
			};
			console.log('tecla: '+lastPress, '| x: '+positionX[active],
			'| y: '+positionY[active], '| active: '+active,
			'| size: '+obj.Characters.length, '| direccion: '+direction[active]);
		});
}

paint();
};
xhr.onreadystatechange = function(e) {
	if (this.readyState == 4 && this.status == 200) {
		(options.success.bind(this))();
	}
};

xhr.open('GET', options.url, true);
xhr.send();
}
}

